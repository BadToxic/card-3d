# Card 3D

![Card 3D](https://gitlab.com/BadToxic/card-3d/raw/master/img/screenshot.png)

A game card with a 3D effect like holographic cards have by [BadToxic](http://badtoxic.de/wordpress)

Shader credits go to [Alan Zucconi](https://www.alanzucconi.com/2015/12/09/3873/)


See the card in action via [WebGL](https://badtoxic.gitlab.io/card-3d/)

Need support? Join my discord server: https://discord.gg/8QMCm2d

Support me with donations, likes and follows:
[Patreon](https://www.patreon.com/badtoxic)
[Instagram](https://www.instagram.com/xybadtoxic)
[Twitter](https://twitter.com/BadToxic)
[YouTube](https://www.youtube.com/user/BadToxic)
[Twitch](https://www.twitch.tv/xybadtoxic)